import axios from "axios";

async function fetchData (endpoint: string, params: object | null = null): Promise<any> {
  try {
    const response = await axios.get(`https://scan-api-stage.cto-tech.amsterdam/${endpoint}`, { params });

    return response;
  } catch (error) {
    console.error(error);

    return new Error();
  }
}

export default function useFetchData () {
  return {
    fetchData,
  };
}
