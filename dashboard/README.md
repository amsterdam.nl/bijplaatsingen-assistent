# Dashboard

The dashboard is built with [NuxtJS](https://nuxtjs.org/), a Vue.js framework specialised in Server Side Rendering. Important components of the dashboard are [Mapbox](https://www.mapbox.com/) and [D3.js](https://d3js.org/).

## Internationalisation

The dashboard is available in two languages: English (fallback) and Dutch (default). The language is set to Dutch by default, but if a string does not exist in Dutch it will fall back to English.

At the moment translatable strings are incuded in Vue's single file components in `<i18n lang="yml">` tags.

## Getting started

### Requirements

- Yarn (or NPM)) + Node (tested with v12.18.1)

### Setup, Run & Build

Navigate to this directory, and:

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build --target static
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Tools

### Code base

- Framework: [NuxtJS](https://nuxtjs.org/) in Universal SSR mode
- Programming: [TypeScript](https://www.typescriptlang.org/), with [ESLint](https://eslint.org/) and `typescript-eslint` for linting
- CSS Framework: [Tailwind CSS](https://tailwindcss.com/), with:
  - [PostCSS](https://postcss.org/), with: `precss`, `postcss-short`, `postcss-each` for syntax and processing
  - [Stylelint](https://stylelint.io/) for linting
- Mapping: [Mapbox GL](https://www.mapbox.com/)
- Charts: [D3.js](https://d3js.org/)
- Unit testing: [Jest](https://jestjs.io/)
- End-to-end testing: TBD
- Internationalisation: `nuxt-i10n`
