import {
  computed,
  ComputedRef,
  ref,
  watchEffect,
} from "@nuxtjs/composition-api";

export default function usePaginate (
  items: ComputedRef<any[]>,
  itemsPerPage: number = 10,
  curPageNum: number = 0,
) {
  const itemsLength = computed(() => items.value.length);

  const currentPageIndex = ref(curPageNum);

  const numberOfPages = computed(() => Math.ceil(itemsLength.value / itemsPerPage));

  const paginatedItems = computed(() => {
    const pageArray = [];

    for (let index = 0; index < numberOfPages.value; index++) {
      const end = index === numberOfPages.value - 1 ? itemsLength.value : itemsPerPage * (index + 1);

      pageArray.push(items.value.slice(itemsPerPage * index, end));
    }

    return pageArray;
  });

  const currentPageOfItems = computed(() => {
    return paginatedItems.value.length > 0 ? paginatedItems.value[currentPageIndex.value] : [];
  });

  function toPage (page: number) {
    currentPageIndex.value = page !== null && page >= 0 ? page : currentPageIndex.value;
  }

  watchEffect(() => {
    currentPageIndex.value = currentPageIndex.value > paginatedItems.value.length - 1 ? paginatedItems.value.length : currentPageIndex.value;
  });

  return {
    numberOfPages,
    currentPageIndex,
    paginatedItems,
    currentPageOfItems,
    toPage,
  };
};
