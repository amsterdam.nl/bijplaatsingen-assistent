import {
  computed,
  reactive,
  ref,
} from "@nuxtjs/composition-api";

const sortState = reactive({
  currentSort: "urgency" as string,
  sortDirs: {
    last_scan: "asc" as string,
    item_cardboard: "asc" as string,
    item_bags: "asc" as string,
    item_bulky: "asc" as string,
    urgency: "asc" as string,
  },
});

let scanObjects = ref([]);

/**
 * Calculate row sortations.
 * @returns { array } Sorted rows.
 */
const sortedScans = computed((): object[] => {
  const modifier: number = sortState.sortDirs[sortState.currentSort] === "desc" ? -1 : 1;

  return scanObjects.value.sort((rowA: any, rowB: any): number => {
    return (rowA[sortState.currentSort].value <= rowB[sortState.currentSort].value ? -1 : 1) * modifier;
  });
});

/**
 * Sort scan objects ascendingly or descendingly.
 * @param { string } sortBy - Data column to sort on.
 */
function sort (sortBy?: string): void {
  if (sortBy) {
    if (sortBy === sortState.currentSort) {
      sortState.sortDirs[sortState.currentSort] = sortState.sortDirs[sortBy] === "asc" ? "desc" : "asc";
    } else {
      sortState.currentSort = sortBy;
    }
  } else {
    sortState.currentSort = "urgency";
    sortState.sortDirs[sortState.currentSort] = "desc";
  }
};

export default function useSorting (unsortedScans?) {
  if (unsortedScans) {
    scanObjects = unsortedScans;
  }

  return {
    sortState,
    sortedScans,
    sort,
  };
};
