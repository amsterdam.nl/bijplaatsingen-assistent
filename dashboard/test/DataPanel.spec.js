import { mount } from "@vue/test-utils";
import AmsOperationalPanelTables from "~/components/Views/Operational/OperationalPanelTables.vue";

describe("Operational", () => {
  test("is a Vue instance", () => {
    const wrapper = mount(AmsOperationalPanelTables);
    expect(wrapper.vm).toBeTruthy();
  });
});
