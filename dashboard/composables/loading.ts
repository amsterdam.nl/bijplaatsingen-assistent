import {
  computed,
} from "@nuxtjs/composition-api";
import store from "~/composables/store";

const { STATE } = store;

const loading = computed(() => {
  return !STATE.loaded.clusters ||
    !STATE.loaded.scans ||
    !STATE.loaded.topLocDistricts ||
    !STATE.loaded.topLoc75
  ;
});

export default function useLoading () {
  return {
    loading,
  };
}
