
function getLocalStorage (key: string) {
  return typeof localStorage !== "undefined" ? localStorage.getItem(key) : null;
}

function setLocalStorage (payload: object) {
  for (const [key, value] of Object.entries(payload)) {
    if (typeof localStorage !== "undefined") {
      localStorage.setItem(key, value);
    } else {
      console.error(`Setting localStorage failed.`);
      console.error(`Key: ${key}`);
      console.error(`Value: ${value}`);
    }
  }
}

export default function useLocalStorage () {
  return {
    getLocalStorage,
    setLocalStorage,
  };
}
