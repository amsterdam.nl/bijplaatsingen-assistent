import { DateTime } from "luxon";

interface CacheObject {
  id: string
  timestamp: Date
  invalidateAt: Date
  data: any
}

let cache: CacheObject[] = [];

/**
 * Remove expired cache entries from the cache array.
 */
function cleanExpired (): void {
  cache = cache.filter(({ invalidateAt }: { invalidateAt: Date }) => DateTime.fromJSDate(invalidateAt) >= DateTime.local());
}

/**
 * Check if requested data exists in cache.
 * @param id { string } Cache object identifier.
 */
function existsInCache (id: string): boolean {
  cleanExpired();

  return typeof cache.find(c => c.id === id) !== "undefined";
}

/**
 * Add fetched date to the cache array.
 * @param id { string } Cache object identifier.
 * @param valid { [number, string] } Amount of time in a given unit that object should expire.
 * @param payload { any[] } The data to be stored.
 */
function addToCache (id: string, valid: [number, string] = [12, "hours"], payload: any[]): void {
  const now = DateTime.local();

  cache.push({
    id,
    timestamp: now.toJSDate(),
    invalidateAt: now.plus({ [valid[1]]: valid[0] }).toJSDate(),
    data: payload,
  });

  console.info(`Fetched ${id} from endpoint and stored in memory.`);
}

/**
 * Retrieve requested date from the cache array.
 * @param id { string } Cache object identifier.
 */
function getFromCache (id: string): any[] {
  const found = cache.find(c => c.id === id);

  console.info(`Loaded ${id} from memory.`);

  return found.data;
}

/**
 * Empty the cache entirely.
 */
function flushCache (): void {
  cache = [];
}

export default function useCache () {
  cleanExpired();

  return {
    existsInCache,
    addToCache,
    getFromCache,
    flushCache,
  };
}
