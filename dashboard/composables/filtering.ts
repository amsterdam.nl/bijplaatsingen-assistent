import {
  computed,
  reactive,
  ref,
  watch,
} from "@nuxtjs/composition-api";
import debounce from "lodash/debounce";
import store from "./store";
import useGlobals from "~/composables/globals";

const { POST_FILTERABLE_PROPS } = useGlobals();

/**
 * Remove non-alphanumeric characters from a string and
 * makes it lowercase for comparison purposes.
 * @param { string } string - String to sanitize.
 */
function sanitize (string: string = "") {
  return string
    .replace(/\W/g, "")
    .toLowerCase()
  ;
}

function filterWithSelect (prevFilter, columnId: string) {
  let filtered = prevFilter.value;

  if (store.STATE.filters.applied?.includes(columnId) && store.STATE.filters.valueFor[columnId] !== "all") {
    filtered = prevFilter.value.filter((row) => {
      return store.STATE.filters.valueFor[columnId] === row[columnId].value;
    });
  }

  return filtered;
}

function filterWithInputText (prevFilter, columnId: string) {
  let filtered = prevFilter.value;

  if (store.STATE.filters.applied?.includes(columnId) && store.STATE.filters.valueFor[columnId] !== "") {
    filtered = prevFilter.value.filter((row) => {
      return sanitize(String(row[columnId].value)).includes(sanitize(store.STATE.filters.valueFor[columnId]));
    });
  }

  return filtered;
}

let scanObjects = ref([]);

const filteredByLocationType = computed((): object[] => filterWithSelect(scanObjects, "location_type"));
const filteredByDistrict = computed((): object[] => filterWithSelect(filteredByLocationType, "district"));
const filteredByArea = computed((): object[] => filterWithInputText(filteredByDistrict, "area"));
const filteredByQuarter = computed((): object[] => filterWithInputText(filteredByArea, "quarter"));
const filteredScans = computed((): object[] => filterWithInputText(filteredByQuarter, "cluster_location"));

/**
 * Filter rows by removal from array.
 */
function filter (value?: string, filterBy?: string | null) {
  const filterablePropValues = {
    daterange: (new Date()) as Date,
    district: "all" as string,
    location_type: "all" as string,
    area: "",
    quarter: "",
    neighbourhood: "",
    cluster_location: "",
  };

  if (typeof value !== "undefined" && filterBy) {
    // User intents to filter on a given value.
    store.filterOn(filterBy, value === "all" ? filterablePropValues[filterBy] : value);

    if (value === "" || value === "all") {
      // User intends to reset one filter.
      console.info(`User intends to reset filter "${filterBy}".`);

      store.filterApply(store.STATE.filters.applied.filter(f => f !== filterBy));
    } else if (filterBy === "district" || filterBy === "daterange") {
      // User requested another district or date.
      console.info(`User requested new data for ${filterBy}.`);

      store.filterApply([filterBy]);
    } else if (!store.STATE.filters.applied?.includes(filterBy)) {
      // Apply new filter as requested.
      console.info(`Apply new filter, ${filterBy}, with value "${value}" as requested.`);

      store.filterApply([...store.STATE.filters.applied, filterBy]);
    } else {
      // Reset all applied filters.
      console.info("Reset all applied filters.");

      store.filterApply([]);
    }
  } else {
    let appliedFilterList = [];

    POST_FILTERABLE_PROPS.forEach((prop) => {
      const index = store.STATE.filters.applied.indexOf(prop);

      if (index > -1) {
        appliedFilterList = [...store.STATE.filters.applied].filter((_, idx) => idx !== index);
      }

      store.filterOn(prop, value || filterablePropValues[prop]);
    });

    store.filterApply(appliedFilterList);
  }
}

const uniqueValuesPerFilterableColumn = reactive({
  location_type: [] as string[],
  area: [] as string[],
  quarter: [] as string[],
  cluster_location: [] as string[],
});

/**
 * Get unique values for a given property (column) in all the row objects.
 * @returns { UniqueValues } Array of strings.
 */
function getUniqueValuesPerFilterableColumn () {
  const propToFilterMap = {
    location_type: scanObjects,
    area: filteredByArea,
    quarter: filteredByQuarter,
    cluster_location: filteredScans,
  };

  POST_FILTERABLE_PROPS.forEach((prop: string) => {
    if (store.STATE.filters.applied.includes(prop)) {
      uniqueValuesPerFilterableColumn[prop] = [...new Set(propToFilterMap[prop].value.map(scan => scan[prop].value))];
    } else {
      uniqueValuesPerFilterableColumn[prop] = [...new Set(filteredScans.value.map(scan => scan[prop].value))];
    }
  });
}

watch(
  () => filteredScans.value,
  debounce(getUniqueValuesPerFilterableColumn, 500, {
    leading: false,
    trailing: true,
  }),
);

export default function useFiltering (unfilteredScans?) {
  if (unfilteredScans) {
    scanObjects = unfilteredScans;
  }

  return {
    filter,
    filteredScans,
    uniqueValuesPerFilterableColumn,
  };
};
