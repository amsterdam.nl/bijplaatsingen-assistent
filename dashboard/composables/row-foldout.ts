import {
  reactive,
} from "@nuxtjs/composition-api";

const foldoutState = reactive({
  openedFilters: false as boolean,
  openedFoldout: null as string | null,
  rowHighlighted: null as string | null,
  markerHighlighted: [] as string[],
  tBody: null as any,
});

export default function useRowFoldout () {
  return {
    foldoutState,
  };
};
