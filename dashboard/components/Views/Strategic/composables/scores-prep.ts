// Util libraries
import cloneDeep from "lodash/fp/cloneDeep";
import { AxiosResponse } from "axios";

// Composables
import useCache from "~/composables/cache";
import useFetchData from "~/composables/fetch-data";

const { existsInCache, addToCache, getFromCache } = useCache();
const { fetchData } = useFetchData();

async function fetchRatings (graph: string = "odk", locationLevel: string | null = null, locationId: string | null = null) {
  const _cacheId = `ratings_${locationLevel}_${locationId}`;

  const datasetDummy = [{
    date: "2020-10-15",
    score: 1,
  }];

  let dataset = [];

  let done = false;

  try {
    if (existsInCache(_cacheId)) {
      dataset = getFromCache(_cacheId);
    } else {
      const endpointLocLvl = `${locationLevel ? `/${locationLevel}` : ""}`;
      const endpointLocId = `${locationId ? `/${locationId}` : ""}`;

      const promisedRatings: AxiosResponse = await fetchData(`graphs/ratings/${graph}${endpointLocLvl}${endpointLocId}`);

      const rawData = await promisedRatings.data;

      const processedData = rawData.map((data) => {
        const dataPoint = cloneDeep(datasetDummy[0]);

        dataPoint.date = data.performed_at;
        dataPoint.score = Math.round(data.urgency);

        return dataPoint;
      });

      addToCache(_cacheId, [2, "hours"], processedData);

      dataset = processedData;
    }

    done = true;
  } catch (error) {
    console.error(error);

    done = true;
  }

  return new Promise((resolve, reject) => {
    try {
      if (done) {
        resolve(dataset);
      }
    } catch (error) {
      reject(error);
    }
  });
}

async function fetchDisplacementNumbers (locationLevel: string | null = null, locationId: string | null = null) {
  const _cacheId = `trashNumbers_${locationLevel}_${locationId}`;

  let dataset = [];

  let done = false;

  try {
    if (existsInCache(_cacheId)) {
      dataset = getFromCache(_cacheId);
    } else {
      const endpointLocLvl = `${locationLevel ? `/${locationLevel}` : ""}`;
      const endpointLocId = `${locationId ? `/${locationId}` : ""}`;

      const promisedNumbers: AxiosResponse = await fetchData(`graphs/bijplaatsingen${endpointLocLvl}${endpointLocId}`);

      const rawData = await promisedNumbers.data;

      addToCache(_cacheId, [2, "hours"], rawData);

      dataset = rawData;
    }

    done = true;
  } catch (error) {
    console.error(error);

    done = true;
  }

  return new Promise((resolve, reject) => {
    try {
      if (done) {
        resolve(dataset);
      }
    } catch (error) {
      reject(error);
    }
  });
}

export default function useTopLocPrep () {
  return {
    fetchRatings,
    fetchDisplacementNumbers,
  };
};
