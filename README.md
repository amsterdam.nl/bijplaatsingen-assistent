# Bijplaatsingen Assistent / Displaced Trash Assistant

The Displaced Trash Assistant, also known as the *Bijplaatsingen Assistent* in Dutch, is a dashboard that allows city managers to gain actionable insights on the realtime status of displaced trash in the city.

The dashboard combines data from the following sources:

- __Object Detection Kit__: scans from the Object Detection Kit app. ODK is an easy way to detect objects on streets with a mobile phone. For more information about the project check [odk.ai](http://www.odk.ai).
- __Trash container locations__: API with all trash container clusters available to be plotted on the map.

## Content

- **Dashboard**: NuxtJS/Vue.js web app showing realtime data from the Object Detection Kit and our sources.

## Getting started

To get the whole stack up and running quickly follow these steps. To run/debug each element seperately, see the README files in their respective folders.

### Requirements

- Docker
- Yarn (or NPM)) + Node (tested with v12.18.1)
- Python (v3.7)

### Setup

See the README in the `dashboard` directory.

### Run

Run docker-compose (dev file) to starts the Dashboard and Middleware:
```
$ docker-compose -f docker-compose.dev.yml up -d
```

Visit [localhost:8000]() to open the ODK Dashboard.

__Alternatively__: do not use Docker, but run the Dashboard directly: see the README in that directory.

## Tools

### Code base

- [VueJS](https://vuejs.org/)

### Infrastructure

- [Docker](https://www.docker.com/) for deployment of services
- [NGINX](https://www.nginx.com/) as proxy & web server
