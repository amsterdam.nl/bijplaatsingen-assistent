import {
  computed,
  ref,
} from "@nuxtjs/composition-api";

// Util libraries
import { DateTime } from "luxon";
import { AxiosResponse } from "axios";

// Composables
import useFetchData from "~/composables/fetch-data";
import useGlobals from "~/composables/globals";
import useCache from "~/composables/cache";
import store from "~/composables/store";
import useSorting from "~/composables/sorting";
import useFiltering from "~/composables/filtering";

// Schemas
import schemaTodayResults from "~/components/Views/Operational/scanResultsTableSchema.json";

const { DISTRICT_ID_MAP } = useGlobals();
const { existsInCache, addToCache, getFromCache } = useCache();
const { fetchData } = useFetchData();

const daysWithScans = ref([]);
const datesHighlighted = computed(() => {
  return {
    dates: daysWithScans.value,
  };
});

const loadedScans = ref([]);

const locationTypeMap = {
  Probleemlocatie: "problem_location",
  Aandachtslocatie: "attention_location",
  "Geen ABP-locatie": "other_location",
};

/**
 * Returns a list of scans prepared for table rendering.
 * @param {object[]} rawScans - List of scans returned by server.
 */
function processRawScans (rawScans: any[]) {
  return rawScans.map((scan) => {
    const address = `${scan.doellocatie.cluster.straatnaam} ${scan.doellocatie.cluster.huisnummer > -1 ? scan.doellocatie.cluster.huisnummer : ""}`;

    const bulkyItems = [
      {
        type: "appliance",
        value: scan.witgoed,
      },
      {
        type: "carpet",
        value: scan.tapijt,
      },
      {
        type: "furniture",
        value: scan.meubels,
      },
      {
        type: "mattress",
        value: scan.matrassen,
      },
      {
        type: "plastic",
        value: scan.plastic,
      },
      {
        type: "wood",
        value: scan.hout,
      },
    ];

    return {
      last_scan: {
        id: "last_scan",
        value: scan.performed_at,
      },
      cluster_location: {
        id: "cluster_location",
        value: address,
        subValue: {
          type: "Feature",
          properties: {
            remote_id: scan.doellocatie.cluster.remote_id,
            address,
            neighbourhood_name: scan.doellocatie.cluster.buurt.naam,
            quarter_name: scan.doellocatie.cluster.buurt.buurtcombinatie.naam,
            area_name: scan.doellocatie.cluster.buurt.buurtcombinatie.gebied.naam,
            district_name: scan.doellocatie.cluster.buurt.buurtcombinatie.stadsdeel.naam,
            cluster_fractie_aantal: {
              residual: scan.doellocatie.cluster.rest_containers,
              bulky: scan.doellocatie.cluster.grof_containers,
              paper: scan.doellocatie.cluster.papier_containers,
              plastic: scan.doellocatie.cluster.plastic_containers,
              pmd: scan.doellocatie.cluster.pmd_containers,
              glass: scan.doellocatie.cluster.glas_containers,
              textile: scan.doellocatie.cluster.textiel_containers,
              bread: scan.doellocatie.cluster.brood_containers,
              gft: scan.doellocatie.cluster.gft_containers,
              other: scan.doellocatie.cluster.anders_containers,
            },
          },
          geometry: scan.doellocatie.cluster.geojson,
        },
      },
      neighbourhood: {
        id: "neighbourhood",
        value: scan.doellocatie.cluster.buurt.naam,
        isHidden: true,
      },
      quarter: {
        id: "quarter",
        value: scan.doellocatie.cluster.buurt.buurtcombinatie.naam,
      },
      area: {
        id: "area",
        value: scan.doellocatie.cluster.buurt.buurtcombinatie.gebied.naam,
      },
      district: {
        id: "district",
        value: scan.doellocatie.cluster.buurt.buurtcombinatie.stadsdeel.naam,
        isHidden: true,
      },
      location_type: {
        id: "location_type",
        value: locationTypeMap[scan.doellocatie.locatietype.naam],
      },
      item_cardboard: {
        id: "item_cardboard",
        value: scan.karton,
      },
      item_bags: {
        id: "item_bags",
        value: scan.vuilniszakken,
      },
      item_bulky: {
        id: "item_bulky",
        value: bulkyItems.reduce((total, cur) => total + cur.value, 0),
        subValue: bulkyItems,
      },
      urgency: {
        id: "urgency",
        value: scan.urgency + (
          scan.karton +
          scan.vuilniszakken +
          scan.witgoed +
          scan.tapijt +
          scan.meubels +
          scan.matrassen +
          scan.plastic +
          scan.hout
        ) / 100,
      },
    };
  });
}

async function fetchDistricts () {
  const _cacheId = "districtCoords";

  try {
    if (existsInCache(_cacheId)) {
      store.setDistricts(getFromCache(_cacheId));
    } else {
      const promisedDistricts: AxiosResponse = await fetchData("stadsdelen");

      const retrievedDistricts = await promisedDistricts.data;

      store.setDistricts(retrievedDistricts);

      addToCache(_cacheId, [28, "days"], retrievedDistricts);
    }
  } catch (error) {
    console.error(error);
  }
}

async function fetchDaysWithScans () {
  const district = store.STATE.filters.valueFor.district;
  const params = {
    stadsdeel_id: district !== "all" ? DISTRICT_ID_MAP[district] : null,
  };

  const _cacheId = `daysWithScansFor_${district}`;

  try {
    if (existsInCache(_cacheId)) {
      daysWithScans.value = await getFromCache(_cacheId);
    } else {
      const promisedDays: AxiosResponse = await fetchData("scan_dates", params);

      daysWithScans.value = await promisedDays.data.map(item => new Date(item));

      addToCache(_cacheId, [12, "hours"], daysWithScans.value);
    }
  } catch (error) {
    console.error(error);
  }
}

async function fetchScans () {
  const district = store.STATE.filters.valueFor.district;
  const datetime = DateTime.fromJSDate(store.STATE.filters.valueFor.daterange || new Date());

  const params = {
    stadsdeel_id: district !== "all" ? DISTRICT_ID_MAP[district] : null,
    most_recent_only: "True",
    date_from: datetime.startOf("day").toISODate(),
    date_to: datetime.plus({ days: 1 }).startOf("day").toISODate(),
  };

  store.setAsLoaded("scans", false);
  loadedScans.value = [];

  const _cacheId = `scans_${district}_${params.date_from}`;
  const requestIsForToday = DateTime.fromISO(params.date_from).hasSame(DateTime.local(), "day");

  try {
    if (existsInCache(_cacheId)) {
      loadedScans.value = await getFromCache(_cacheId);
    } else {
      const rawScans: AxiosResponse = await fetchData("scans", params);

      loadedScans.value = await processRawScans(rawScans.data);

      addToCache(_cacheId, requestIsForToday ? [10, "minutes"] : [7, "days"], loadedScans.value);
    }
  } catch (error) {
    loadedScans.value = [];

    console.error(error);
  }
  store.setAsLoaded("scans", true);
}

// Mark some columns as hideable in case of a too narrow viewport.
const includesHideableColumns = computed((): object => {
  // @ts-ignore
  const hideableColumns = Object.values(schemaTodayResults[0].rows[0]).filter(c => c.hideable).map(c => c.id);
  const obj = {};

  Object.keys(schemaTodayResults[0].rows[0]).forEach((colRef: string) => {
    obj[colRef] = hideableColumns.includes(colRef);
  });

  return obj;
});

// Scan object arrays are passed through filtering and sorting.
const { filteredScans } = useFiltering(loadedScans);
const { sortedScans } = useSorting(filteredScans);
const preparedScanRows = computed(() => sortedScans.value);

// Number of scan objects in the unfiltered scans array.
const numberOfScans = computed((): number => loadedScans.value.length);

// Number of scan objects in the filtered scans array.
const numberOfFilteredScans = computed((): number => preparedScanRows.value.length);

export default function useScansPrep () {
  return {
    datesHighlighted,
    fetchDaysWithScans,
    fetchDistricts,
    fetchScans,
    includesHideableColumns,
    tableGroups: schemaTodayResults,
    preparedScanRows,
    numberOfScans,
    numberOfFilteredScans,
  };
};
