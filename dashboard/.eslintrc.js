module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    "@nuxtjs/eslint-config-typescript",
    // "plugin:nuxt/recommended",
  ],
  plugins: [
  ],
  rules: {
    "comma-dangle": [2, "always-multiline"],
    "no-console": [1, { allow: ["info", "warn", "error"] }],
    "no-debugger": 1,
    "prefer-const": 1,
    quotes: [2, "double", {
      allowTemplateLiterals: true,
    }],
    semi: [2, "always"],
    "space-before-function-paren": 1,
    "vue/attributes-order": [1, {
      order: [
        "DEFINITION",
        "LIST_RENDERING",
        "CONDITIONALS",
        "RENDER_MODIFIERS",
        "GLOBAL",
        "UNIQUE",
        "TWO_WAY_BINDING",
        "OTHER_DIRECTIVES",
        "OTHER_ATTR",
        "EVENTS",
        "CONTENT",
      ],
    }],
    "vue/max-attributes-per-line": [2, {
      singleline: 3,
      multiline: {
        max: 1,
        allowFirstLine: false,
      },
    }],
    "vue/require-component-is": 1,
    "vue/singleline-html-element-content-newline": 1,
  },
};
