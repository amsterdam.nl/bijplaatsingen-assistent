import {
  reactive,
  readonly,
} from "@nuxtjs/composition-api";
import merge from "lodash/fp/merge";
import useLocalStorage from "./local-storage";

interface District {
  "code": string;
  "naam": string;
  "created_at": string;
  "last_modified_at": string | null;
  "bounding_box": number[][];
  "id": number;
}

interface ClusterId {
  "id": number;
  "remote_id": string;
  "address": string;
}

interface State {
  loaded: {
    clusters: boolean;
    scans: boolean;
    topLocDistricts: boolean;
    topLoc75: boolean;
  };
  districts: District[] | null;
  clusterIds: ClusterId[];
  filters: {
    valueFor: {
      daterange: Date;
      district: string;
      "location_type": string;
      area: string;
      quarter: string;
      neighbourhood: string;
      "cluster_location": string;
    };
    applied: string[];
  };
  enableMapSwitch: boolean;
  rightPanelOpen: boolean;
}

//
// State
//

const state: State = reactive({
  loaded: {
    clusters: true,
    scans: true,
    topLocDistricts: true,
    topLoc75: true,
  },
  districts: null,
  clusterIds: [],
  filters: {
    valueFor: {
      daterange: (new Date()),
      district: "all",
      location_type: "all",
      area: "",
      quarter: "",
      neighbourhood: "",
      cluster_location: "",
    },
    applied: [],
  },
  enableMapSwitch: true,
  rightPanelOpen: true,
} as State);

//
// Actions
//

/**
 * Set one filter state.
 * @param key { string }
 * @param value { string | Date }
 */
function filterOn (key: string, value: string | Date): void {
  state.filters.valueFor[key] = value;
}

/**
 * Apply all of the filter state.
 * @param list { string[] }
 */
function filterApply (list: string[]): void {
  state.filters.applied = list;
}

/**
 * Mark or unmark something as loaded in the store.
 * @param key { string }
 * @param isLoaded { boolean }
 */
function setAsLoaded (key: keyof State["loaded"], isLoaded: boolean = false): void {
  state.loaded[key] = isLoaded;
}

/**
 * Store districts in the store.
 * @param districtData { District[] | null }
 */
function setDistricts (districtData: District[] | null = null): void {
  state.districts = districtData;
}

/**
 * Store cluster identifiers in object.
 * @param id { number }
 * @param remoteId { string }
 * @param address { string }
 */
function setClusterId (id: number, remoteId: string, address: string): void {
  state.clusterIds[id] = {
    id,
    remote_id: remoteId,
    address,
  };
}

/**
 * Enable or disable switching the map.
 * @param enable { boolean }
 */
function enableMapSwitch (enable: boolean = false): void {
  state.enableMapSwitch = enable;
}

/**
 * Open or close the right panel.
 * @param open { boolean }
 */
function switchRightPanel (open: boolean = false): void {
  state.rightPanelOpen = open;
}

//
// Initialize store to store state in localStorage on a given interval.
//

function init (): void {
  let storeInterval = null;

  const { getLocalStorage, setLocalStorage } = useLocalStorage();

  const locallyStored = JSON.parse(getLocalStorage("dashboardState") || "");

  if (locallyStored) {
    Object.keys(locallyStored).forEach((key: keyof State) => {
      state[key] = merge(state[key], locallyStored[key]);
    });
  }

  if (storeInterval) {
    clearInterval(storeInterval);
  }

  storeInterval = setInterval((): void => {
    const stateSubsetForLocalStorage = {
      districts: state.districts,
      filters: {
        valueFor: {
          district: state.filters.valueFor.district,
        },
      },
    };

    const storeSubsetString = JSON.stringify(stateSubsetForLocalStorage);

    if (getLocalStorage("dashboardState") !== storeSubsetString) {
      console.info("Storing user data in localStorage.");

      setLocalStorage({
        dashboardState: storeSubsetString,
      });
    }
  }, 30000);
}

export default {
  // State
  STATE: readonly(state),

  // Actions
  setAsLoaded,
  setDistricts,
  setClusterId,
  filterOn,
  filterApply,
  enableMapSwitch,
  switchRightPanel,

  // Initialize
  init,
};
