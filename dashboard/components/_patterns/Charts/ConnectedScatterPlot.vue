<template>
  <div
    ref="chartWrapper"
    class="chart"
  >
    <slot name="graphtitle" />

    <svg
      xmlns="http://www.w3.org/2000/svg"
      :width="svgSize.width"
      :height="svgSize.height"
    >
      <g
        ref="chartArea"
        :transform="`translate(${dimensions.margins.left}, ${dimensions.margins.top})`"
      >
        <g
          ref="chartAxisBottom"
          :transform="`translate(0, ${dimensions.height})`"
          class="axis x bottom"
        />

        <g
          ref="chartAxisLeft"
          class="axis y left"
        />
      </g>
    </svg>

    <slot name="labels" />
  </div>
</template>

<script lang="ts">
import {
  computed,
  defineComponent,
  onMounted,
  reactive,
  ref,
  watch,
} from "@nuxtjs/composition-api";

// Utility libraries
import debounce from "lodash/debounce";
import { DateTime } from "luxon";

// D3.js
import * as d3Array from "d3-array";
import * as d3Axis from "d3-axis";
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";
import * as d3Select from "d3-selection";
import * as d3Time from "d3-time";
import * as d3TimeFormat from "d3-time-format";
import * as d3Transition from "d3-transition";

// Composables
import useGlobals from "~/composables/globals";

// Combine cherry-picked modules into new d3 object.
const d3 = Object.assign({}, d3Array, d3Axis, d3Scale, d3Shape, d3Select, d3Time, d3TimeFormat, d3Transition);

const __MOCK_NOW__ = "2020-12-10";

export default defineComponent({
  name: "AmsConnectedScatterPlot",

  props: {
    datasets: {
      type: Array,
      default: () => [],
    },
    xAxis: {
      type: Object,
      required: true,
      default: () => {
        return {
          label: "Date",
          id: "date",
          type: "date",
          domain: [DateTime.fromISO(__MOCK_NOW__).minus({ days: 56 }), DateTime.fromISO(__MOCK_NOW__)],
          format: "week",
        };
      },
    },
    yAxis: {
      type: Object,
      default: () => {
        return {
          label: "Score",
          id: "score",
          type: "string",
          domain: ["A+", "A", "B", "C", "D"],
          tickValues: null,
          scale: "band",
        };
      },
    },
    thresholdMarker: {
      type: Object,
      default: null,
    },
  },

  setup (props) {
    // Element refs
    const d3Svg = ref(null);
    const chartWrapper = ref(null);
    const chartArea = ref(null);
    const chartAxisLeft = ref(null);
    const chartAxisBottom = ref(null);

    const { COLORS } = useGlobals();
    const RATIO = 0.3675;

    const thresholdMarker = [null, null, null];

    let xAxis = null;
    let yAxis = null;

    const dimensions = reactive({
      margins: { top: 60, right: 30, bottom: 30, left: 60 },
      width: 0,
      height: 0,
    });

    const svgSize = computed(() => {
      return {
        width: dimensions.width + dimensions.margins.left + dimensions.margins.right,
        height: dimensions.height + dimensions.margins.top + dimensions.margins.bottom,
      };
    });

    const datasets = computed(() => props.datasets);

    function parseDates (data: any[]) {
      return data.map((d) => {
        const obj: any = {};

        obj.date = DateTime.fromISO(d[props.xAxis.id]);
        obj[props.yAxis.id] = d[props.yAxis.id];

        return obj;
      });
    }

    function drawOrUpdateThresholdMarker (date: Date, labels: string[]) {
      const drawLabel = (index: number, labelText: string, labelYOffset: number, textProps = {
        size: 12,
        weight: "bold",
        color: "black",
      }) => {
        thresholdMarker[index] = d3Svg.value
          .insert("text", ":first-child")
          .attr("class", "cyclestart")
          .attr("x", () => xAxis(date) + 8)
          .attr("y", labelYOffset)
          .attr("dy", 16)
          .attr("color", textProps.color)
          .attr("font-weight", textProps.weight)
          .attr("font-size", textProps.size)
          .text(labelText);
        ;
      };

      // Vertical threshold line.
      thresholdMarker[0] = d3Svg.value
        .insert("line", ":first-child")
        .attr("class", "cyclestart")
        .attr("stroke", COLORS.gray[400])
        .attr("stroke-width", 2)
        .attr("stroke-dasharray", 6)
        .attr("x1", () => xAxis(date))
        .attr("y1", 0 - dimensions.margins.top + 20)
        .attr("x2", () => xAxis(date))
        .attr("y2", dimensions.height)
      ;

      // Text labels on the line.
      drawLabel(1, labels[0], 0 - dimensions.margins.top + 16);
      drawLabel(2, labels[1], 0 - dimensions.margins.top + 32, {
        size: 10,
        weight: "normal",
        color: COLORS.gray[400],
      });
    }

    function drawOrUpdateXAxis (xAxisProps) {
      xAxis = d3.scaleTime()
        .domain(xAxisProps.domain
          ? xAxisProps.domain
          : d3.extent(
            datasets.value.map((set: any) => parseDates(set.data))[0],
            (d: any) => d[xAxisProps.id]),
        )
        .range([0, dimensions.width])
      ;

      d3.select(chartAxisBottom.value)
        .transition(d3.transition().duration(750))
        .call(d3.axisBottom(xAxis)
          .tickFormat((d: Date) => {
            return xAxisProps.format === "week" ? `week ${DateTime.fromJSDate(d).weekNumber}` : `${DateTime.fromJSDate(d).toFormat("EEE dd")}`;
          }),
        )
        .selectAll("line").remove()
      ;

      d3.select(chartAxisBottom.value)
        .selectAll(".tick")
        .attr("x", 30)
      ;

      if (thresholdMarker.every(t => t !== null)) {
        d3.selectAll("line.cyclestart")
          .transition(d3.transition().duration(750))
          .attr("x1", () => xAxis(props.thresholdMarker.date))
          .attr("x2", () => xAxis(props.thresholdMarker.date))
        ;

        d3.selectAll("text.cyclestart")
          .transition(d3.transition().duration(750))
          .attr("x", () => xAxis(props.thresholdMarker.date) + 8)
        ;
      }
    }

    function drawOrUpdateYAxis (yAxisProps) {
      // Create Y axis
      switch (yAxisProps.scale) {
        case "band":
          yAxis = d3.scaleBand()
            .paddingInner(1)
            .paddingOuter(0.5);
          break;
        case "linear":
          yAxis = d3.scaleLinear();
          break;
        default:
          yAxis = d3.scaleBand()
            .paddingInner(1)
            .paddingOuter(0.5);
          break;
      }

      yAxis = yAxis
        .domain(yAxisProps.domain || d3.extent(datasets[0].data, d => d[yAxisProps.id]))
        .range([dimensions.height, 0])
      ;

      // Append Y axis
      d3.select(chartAxisLeft.value)
        .call(
          d3.axisLeft(yAxis)
            .ticks(yAxisProps.ticks)
            .tickValues(yAxisProps.tickValues)
            .tickSize(-dimensions.width + 15, 0, 0),
        )
        .selectAll("text")
        .attr("x", -30)
        .attr("text-anchor", "start")
      ;

      d3.selectAll(".tick").attr("class", "tick");

      d3.select(chartAxisLeft.value)
        .selectAll("line")
        .attr("transform", "translate(5)")
      ;
    }

    function drawOneScatterPlot (id, color, data) {
      return d3Svg.value
        .append("g")
        .attr("class", `point-group point-group-${id}`)
        .selectAll("point")
        .data(data)
        .enter().append("circle")
        .attr("class", "point")
        .attr("fill", color)
        .attr("r", 7)
        .attr("cx", d => xAxis(d[props.xAxis.id]))
        .attr("cy", d => yAxis(d[props.yAxis.id]))
      ;
    }

    function drawOneLine (id, color, data) {
      return d3Svg.value.append("path")
        .datum(data)
        .attr("class", `line line-${id}`)
        .attr("d", d3.line()
          .x(d => xAxis(d[props.xAxis.id]))
          .y(dimensions.height))
        .attr("fill", "none")
        .attr("stroke", color)
        .attr("stroke-width", 3)
        .attr("stroke-opacity", 0)
        .transition(d3.transition().duration(750))
        .attr("d", d3.line()
          .x(d => xAxis(d[props.xAxis.id]))
          .y(d => yAxis(d[props.yAxis.id])))
        .attr("stroke-opacity", 1)
      ;
    }

    function initGraphs () {
      const wrapperWidth = chartWrapper.value.clientWidth;

      dimensions.width = wrapperWidth - dimensions.margins.left - dimensions.margins.right;
      dimensions.height = dimensions.width * RATIO - dimensions.margins.top - dimensions.margins.bottom;

      d3Svg.value = d3.select(chartArea.value);

      if (datasets.value && datasets.value[0]) {
        const parsedDataSets = datasets.value.map((set: any) => {
          return {
            id: set.id,
            color: set.color,
            data: parseDates(set.data),
            visible: set.visible,
          };
        });

        drawOrUpdateXAxis(props.xAxis);
        drawOrUpdateYAxis(props.yAxis);

        if (props.thresholdMarker) {
          drawOrUpdateThresholdMarker(props.thresholdMarker.date, [props.thresholdMarker.title, props.thresholdMarker.subTitle]);
        }

        parsedDataSets.forEach(({ id, color, data, visible }) => {
          drawOneLine(id, color, visible ? data : []);
          drawOneScatterPlot(id, color, visible ? data : []);
        });
      }
    }

    function updateGraphs () {
      datasets.value.forEach(({ id, color, data, visible }) => {
        const parsedDataSets = parseDates(visible ? data : []);

        const line = d3Svg.value.select(`path.line-${id}`).datum(parsedDataSets);

        line.exit().transition(d3.transition().duration(250)).style("opacity", 0).remove();

        line
          .transition(d3.transition().duration(250))
          .attr("d", d3.line()
            .x(d => xAxis(d[props.xAxis.id]))
            .y(d => yAxis(d[props.yAxis.id])),
          )
          .attr("stroke", color)
        ;

        const circles = d3Svg.value.select(`g.point-group-${id}`).selectAll("circle.point").data(parsedDataSets);

        circles.exit()
          .transition(d3.transition().duration(250))
          .style("opacity", 0)
          .remove()
        ;

        circles.enter().append("circle")
          .attr("class", "point")
          .attr("fill", color)
          .attr("fill-opacity", 0)
          .attr("r", 7)
          .attr("cx", d => xAxis(d[props.xAxis.id]))
          .attr("cy", d => yAxis(d[props.yAxis.id]))
          .transition(d3.transition().duration(250))
          .attr("fill", color)
          .attr("fill-opacity", 1)
        ;

        circles
          .transition(d3.transition().duration(250))
          .attr("cx", d => xAxis(d[props.xAxis.id]))
          .attr("cy", d => yAxis(d[props.yAxis.id]))
          .attr("fill", color)
          .attr("fill-opacity", 1)
        ;
      });
    }

    function resetGraphs () {
      d3Svg.value.selectAll(".cyclestart").remove();
      d3Svg.value.selectAll("path.line").remove();
      d3Svg.value.selectAll("g.point-group").remove();

      initGraphs();
    }

    onMounted(() => {
      setTimeout(initGraphs, 500);

      watch(() => datasets.value, debounce(updateGraphs, 500));

      watch(() => props.xAxis, debounce((newXAxis) => {
        drawOrUpdateXAxis(newXAxis);
        updateGraphs();
      }, 500));

      watch(() => props.yAxis, debounce((newYAxis) => {
        drawOrUpdateYAxis(newYAxis);
        updateGraphs();
      }, 500));

      watch(() => props.thresholdMarker, debounce((newThresholdMarker) => {
        drawOrUpdateThresholdMarker(newThresholdMarker.date, [newThresholdMarker.title, newThresholdMarker.subTitle]);
        updateGraphs();
      }, 500));

      // @ts-ignore
      window.addEventListener("resize", debounce(resetGraphs, 500));
    });

    return {
      chartWrapper,
      chartArea,
      chartAxisLeft,
      chartAxisBottom,
      dimensions,
      svgSize,
    };
  },
});
</script>

<style lang="postcss">
.chart {
  margin-top: 20px;

  @apply border-2 border-gray-150 w-full bg-white shadow-sm;

  .axis {
    &.y {
      .tick text {
        @apply text-sm;
      }
    }

    & > path {
      @apply invisible;
    }

    .tick {
      line {
        stroke: currentColor;

        @apply text-gray-200;
      }

      text {
        @apply text-xs text-gray-700 font-sans;
      }
    }
  }

  .cyclestart {
    @apply fill-current text-xs font-bold;
  }
}
</style>
