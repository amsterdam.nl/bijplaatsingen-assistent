/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  prefix: "",
  important: false,
  separator: ":",
  theme: {
    borderWidth: {
      ...defaultTheme.borderWidth,
      1: "1px",
      6: "6px",
    },
    colors: {
      black: "#000",
      blue: {
        300: "#ccdaeb",
        500: "#0044dd",
        700: "#004699",
      },
      gray: {
        100: "#fafafa",
        150: "#f5f5f5",
        200: "#e6e6e6",
        400: "#b4b4b4",
        600: "#767676",
        700: "#4f4f4f",
        900: "#2f2f2f",
      },
      green: {
        300: "#daf1e3",
        800: "#00a03c",
      },
      orange: {
        300: "#ffefda",
        800: "#ff9100",
      },
      red: {
        300: "#fddada",
        800: "#ec0000",
      },
      pink: {
        800: "#e50082",
      },
      transparent: "transparent",
      white: "#fff",
    },
    fontFamily: {
      ...defaultTheme.fontFamily,
      sans: [
        "AvenirNextLTPro",
        "system-ui",
        "-apple-system",
        "BlinkMacSystemFont",
        "'Segoe UI'",
        "Roboto",
        "'Helvetica Neue'",
        "Arial",
        "'Noto Sans'",
        "sans-serif",
        "'Apple Color Emoji'",
        "'Segoe UI Emoji'",
        "'Segoe UI Symbol'",
        "'Noto Color Emoji'",
      ],
    },
    fontSize: {
      xs: "0.75rem",
      sm: "0.875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
    },
    screens: {
      ...defaultTheme.screens,
      x2l: "2300px", // => @media (min-width: 1280px) { ... }
    },
  },
  variants: {},
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: true, // process.env.NODE_ENV === "production",
    content: [
      "components/**/*.vue",
      "layouts/**/*.vue",
      "pages/**/*.vue",
      "plugins/**/*.js",
      "plugins/**/*.ts",
      "nuxt.config.js",
    ],
    options: {
      whitelist: [
        "bg-gray-100",
        "bg-gray-150",
        "bg-gray-200",
        "bg-gray-400",
        "bg-red-800",
        "bg-orange-800",
        "bg-green-800",
        "text-gray-100",
        "text-gray-150",
        "text-gray-200",
        "text-gray-400",
        "text-red-800",
        "text-orange-800",
        "text-green-800",
      ],
    },
  },
};
